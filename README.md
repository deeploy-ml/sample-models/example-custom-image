# AlexNet model as Custom Image

AlexNet is the name of a convolutional neural network (CNN) architecture, designed by Alex Krizhevsky in collaboration with Ilya Sutskever and
Geoffrey Hinton, who was Krizhevsky's Ph.D. advisor. AlexNet competed in the ImageNet Large Scale Visual Recognition Challenge on September 30, 2012.
The network achieved a top-5 error of 15.3%, more than 10.8 percentage points lower than that of the runner up. The original paper's primary result
was that the depth of the model was essential for its high performance, which was computationally expensive, but made feasible due to the utilization
of graphics processing units (GPUs) during training.

---

This is an example repository that can be used as a reference to deploy a custom image using Deeploy.
Original: [KServe custom model](https://github.com/kserve/kserve/tree/release-0.9/docs/samples/v1beta1/custom/custom_model)

> An example input to interact with the model can be found in `samples/example_input.json`

## Steps

1. Create a custom Docker image
2. Build and push the sample Docker Image

## Create a custom Docker image

The goal of custom image support is to allow users to bring their own wrapped model inside a container and serve it with Deeploy. Please note that you
will need to ensure that your container is also running a web server e.g. Flask to expose your model endpoints. This example extends KFServing.KFModel
which uses the tornado web server.

## Build and push the sample Docker Image

In this example we use Docker to build the sample python server into a container. To build and push with Docker Hub, run these commands replacing
{username} with your Docker Hub username:

```bash
# Build the container on your local machine
docker build -t {username}/kserve-custom-model ./model-server

# Push the container to docker registry
docker push {username}/kserve-custom-model
```

