import base64
import json

with open("example_input.json") as json_file:
    sample_request = json.load(json_file)

base64_data = sample_request["instances"][0]["image"]["b64"]
base64_encoded_data = base64_data.encode('utf-8')

with open('sample_image.jpg', 'wb') as file_to_save:
    decoded_image_data = base64.decodebytes(base64_encoded_data)
    file_to_save.write(decoded_image_data)