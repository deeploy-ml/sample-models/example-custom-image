import base64
import json

with open("alexnet_example_dog.jpg", "rb") as image_file:
    base64_encoded_data = base64.b64encode(image_file.read())
    base64_data = base64_encoded_data.decode('utf-8')

sample_request = {
    "instances": [{
        "image": {
            "b64": base64_data
        }
    }]
}

with open('sample_request.json', 'w', encoding='utf-8') as f:
    json.dump(sample_request, f, ensure_ascii=False, indent=4)